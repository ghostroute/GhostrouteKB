Developer documentation
=======================

This section of the documentation is targeted at anyone doing further
development of the software stack.

Auto-generated API documentation
--------------------------------

The API documentation, as far as it exists, can be found at
[API doc endpoint](../developer/API/).


Documentation workflow
-----------------------

The documentation your are reading is hosted in a project-wide or site-wide
`git` repository (probably the project's monorepos or primary repository)
as a bunch of (markdown) documents. This allows the documentation to follow
the code across git branches and merges, keeps it more up-to-date, and
allows an easy record of what the documentation looked like at the time of
release.

When adding new documentation, please try to follow the general structure
of enduser- / admin- / developer-specific sections.


Additional resources
--------------------

 * [administrator documentation](../admin)

